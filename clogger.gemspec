manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{clogger}
  s.version = (ENV['VERSION'] || '2.4.0').dup
  s.homepage = 'https://YHBT.net/clogger/'
  s.authors = ["cloggers"]
  s.summary = 'configurable request logging for Rack'
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{clogger-public@YHBT.net}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.test_files = %w(test/test_clogger.rb test/test_clogger_to_path.rb)

  # HeaderHash wasn't case-insensitive in old versions
  s.add_dependency(%q<rack>, ['>= 1.0', '< 4.0'])
  s.add_development_dependency('test-unit', '~> 3.0')
  s.add_development_dependency('rackup', '~> 2.0')
  s.extensions = %w(ext/clogger_ext/extconf.rb)
  s.licenses = %w(LGPL-2.1-or-later)
end
